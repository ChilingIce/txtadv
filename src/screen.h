#ifndef SCREEN_H	
#define SCREEN_H

#include <ncurses.h>
#include <string>

using namespace std;

class screenProcess {
	public:
		screenProcess();
		void setpos(int y, int x);
		void setpos(string var, char action, int val);
		void limit();
};

class screenRender {
	public:
		screenRender();
		void init(bool spawn);
		void rogue_border(char ls, char rs, char ts, char bs, char tl, char tr, char bl, char br);
		void rogue_border(WINDOW *win, char ls, char rs, char ts, char bs, char tl, char tr, char bl, char br);
		void player();
		void player(int y, int x);
};

class Screen {
	public:
		screenProcess *process;
		screenRender *render;

		Screen(screenProcess *p, screenRender *r);

		void initbnd(int mn_x, int mn_y, int mx_x, int mx_y);
		void initchar(char player, int y, int x);
		int getint(string var);
		char getchar(string var);
		bool getbool(string var);
		WINDOW* getwindow(string var);
		
		static WINDOW *rogue_window/*, console_window, info_window, menu_window*/;
		static bool initspawn;
		static char player;
		static int line, column;
		static int maximum_x, maximum_y;
		static int minimum_x, minimum_y;
};

#endif