#include <iostream>
#include <string>
#include <cxxopts.hpp>
#include <curses.h>
#include "screen.h"

using namespace std;
using namespace cxxopts;

int scolumn = 1, sline = 1;   //initial position
int mx_x = 27, mx_y = 9;   //maximum bounds
int mn_x = 0, mn_y = 0;   //minimum x and y 
char player_char = '@';   //player char
bool spawnoninit = true;

screenProcess process;
screenRender render;

Screen screen(&process, &render);

cxxopts::Options arguments("txtadv -p char -l int -c int", "");

void initargs() {
    arguments.add_options("Standard args")
            ("h,help", "Print help")
            ("p,player", "Specify char used for player character", cxxopts::value<char>()
                ->default_value("@")->implicit_value("@"))
            ("l,line", "initial y starting value", cxxopts::value<int>()
                ->default_value("1")->implicit_value("1"))
            ("c,column", "initial x starting value", cxxopts::value<int>()
                ->default_value("1")->implicit_value("1"))
        ;
}

int main(int argc, char *argv[]) {
    //Parse arguments using cxxopts library
    try {
        
        initargs();

        arguments.parse(argc, argv);

        if(arguments.count("h")) {
            std::cout << arguments.help({"", "Standard args"}) << std::endl;
            exit(0);
        }

        if(arguments.count("p")) {
            player_char = arguments["p"].as<char>();
            } /*else {
                player_char = arguments["p"].as<char>();
                std::cout << "error parsing player char arg" << std::endl;
                exit(0);
        }*/

        //Note: -r and -c must come after -p in order to work
        if(arguments.count("l")) {
            sline = arguments["l"].as<int>();
            } /*else {
                sline = arguments["r"].as<int>();
                std::cout << "error parsing line arg" << std::endl;
                exit(0);
        } */

        if(arguments.count("c")) {
            scolumn = arguments["c"].as<int>();
            } /*else {
                sline = arguments["c"].as<int>();
                std::cout << "error parsing column arg" << std::endl;
                // exit(0);
        }*/
    } catch (const cxxopts::OptionException& e) {
            std::cout << "error parsing options: " << e.what() << std::endl;
            exit(1);
    }

    // Start ncurses
    screen.initbnd(mn_x, mn_y, mx_x, mx_y);
    screen.initchar(player_char, sline, scolumn);

    screen.render->init(spawnoninit);

    // Start the game loop
    if(screen.getbool("initspawn") == false) {
        screen.render->player();
        screen.render->rogue_border(screen.getwindow("rogue"), '#', '#', '-', '-', '%', '%', '%', '%');
        refresh();
    } else refresh();

    for(;;){
        int key = wgetch(screen.getwindow("rogue"));   

        /*
            Keyboard input to move in a 
            specific direction with the arrow keys
        */  
        if(key == KEY_LEFT) {
            screen.process->setpos("column", '-', 1);
        }
        else if(key == KEY_RIGHT) {
            screen.process->setpos("column", '+', 1);
        }
        else if(key == KEY_UP) {
            screen.process->setpos("line", '-', 1);
        }
        else if(key == KEY_DOWN) {
            screen.process->setpos("line", '+', 1);
        }

        /*
            screenProcess function to limit where 
            the character (/player character) can travel 
        */
        screen.process->limit();

        /*
            Character is now ready to render. Shift 
            the character, clear the terminal and 
            refresh the terminal
        */
        //clear();
        screen.render->rogue_border(screen.getwindow("rogue"), '#', '#', '-', '-', '%', '%', '%', '%');
        screen.render->player();
        refresh(/*screen.getwindow("rogue")*/);
    }

    // Clear ncurses data structures
    endwin();
    return 0;
}