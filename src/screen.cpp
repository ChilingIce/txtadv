#include <ncurses.h>
#include "screen.h"

int Screen::line = 0, Screen::Screen::column = 0;
int Screen::minimum_x = 0, Screen::minimum_y = 0;
int Screen::maximum_x = 0, Screen::maximum_y = 0; 

WINDOW* Screen::rogue_window/*, console_window, info_window, menu_window*/;

char Screen::player = ' ';
bool Screen::initspawn;

//Screen
//Constructor for Screen class
Screen::Screen(screenProcess *p, screenRender *r) : process(p), render(r) {}

/* Screen
    Seperation of "construction" of the screen class 
    so that minx/y and max-x/y can be modified on the fly
*/
void Screen::initbnd(int mn_x, int mn_y, int mx_x, int mx_y)
{
    minimum_x = mn_x;
    minimum_y = mn_y;
    maximum_x = mx_x;
    maximum_y = mx_y;
}

/* Screen
    Seperation of "construction" of the 
    screen class so that the main_char and 
    starting x and y values can be modified on the fly
*/
void Screen::initchar(char main_char, int y, int x)
{
    player = main_char;
    line = y;
    column = x;
}

/* Screen
    Retrieves a specified int value 
    from an instance of the Scene 
    class. getchar can only retrieve 
    chars specified in getbool.
*/
int Screen::getint(string var)
{
    if(var == "line") return Screen::line;
    if(var == "column") return Screen::column;
    if(var == "minimumy") return Screen::minimum_y;
    if(var == "minimumx") return Screen::minimum_x;
    if(var == "maximumy") return Screen::maximum_y;
    if(var == "maximumx") return Screen::maximum_x;
    else return NULL;
}

/* Screen
    Retrieves a specified char value 
    from an instance of the Scene 
    class. getchar can only retrieve 
    chars specified in getbool.
*/
char Screen::getchar(string var)
{
    if(var == "player" || var == "main_char") return Screen::player;
    else return NULL;
}

/* Screen
    Retrieves a specified boolean value 
    from an instance of the Scene class.
    getbool can only retrieve strings 
    specified in getbool.
*/
bool Screen::getbool(string var)
{
    if(var == "initspawn" || var == "spawn") return Screen::initspawn;
    else return NULL;
}

WINDOW* Screen::getwindow(string var)
{
    if(var == "rogue") return Screen::rogue_window;
    else return NULL;
}

/* screenProcess
    Constructor for screenProcess class
*/
screenProcess::screenProcess(){}

/* screenProcess
    setpos changes the internal 
    y and x (aka line and column) values  
*/
void screenProcess::setpos(int y, int x)
{
    Screen::line = y;
    Screen::column = x;
}

void screenProcess::setpos(string var, char action, int val)
{
    if(var == "y" || "line") {
        if(action == '+') Screen::line += val;
        if(action == '-') Screen::line -= val;
    } 
    if(var == "x" || "column" ) {
        if(action == '+') Screen::column += val;
        if(action == '-') Screen::column -= val;
    }
}

/* screenProcess
    Function to define 
    basic screen boundaries
*/
void screenProcess::limit()
{
	if(Screen::column+1 >= Screen::maximum_x)
    {
    	Screen::column = Screen::maximum_x-1;
    }
    else if(Screen::column-1 <= Screen::minimum_x)
    {
    	Screen::column = Screen::minimum_x+1;
    }

    if(Screen::line+1 >= Screen::maximum_y)
    {
    	Screen::line = Screen::maximum_y-1;
    }
    else if(Screen::line-1 <= Screen::minimum_y)
    {
    	Screen::line = Screen::minimum_y + 1;
    }
}

/* screenRender
    Constructor for screenRender class
*/
screenRender::screenRender(){} 

/* ncurses 
    Initialize the ncurses library
    Allows user to decide if char is 
    placed when screen is inited
*/
void screenRender::init(bool spawn)
{
    Screen::initspawn = spawn;
    initscr();
    noecho();
    cbreak();
    keypad(stdscr, TRUE);
    curs_set(0);
    Screen::rogue_window = newwin(Screen::maximum_y, Screen::maximum_x, Screen::line, Screen::column);
    wborder(Screen::rogue_window, '#', '#', '-', '-', '%', '%', '%', '%');
    /* The parameters f taken are 
      1. win: the window on which to operate
      2. ls: character to be used for the left side of the window 
      3. rs: character to be used for the right side of the window 
      4. ts: character to be used for the top side of the window 
      5. bs: character to be used for the bottom side of the window 
      6. tl: character to be used for the top left corner of the window 
      7. tr: character to be used for the top right corner of the window 
      8. bl: character to be used for the bottom left corner of the window 
      9. br: character to be used for the bottom right corner of the window
     */
    if(Screen::initspawn == true)
    {
        mvwaddch(Screen::rogue_window, Screen::line, Screen::column, Screen::player); // Show the main character on the screen
        refresh();
    }
}

void screenRender::rogue_border(char ls, char rs, char ts, char bs, char tl, char tr, char bl, char br)
{
    wborder(Screen::rogue_window, ls, rs, ts, bs, tl, tr, bl, br);
}

void screenRender::rogue_border(WINDOW *win, char ls, char rs, char ts, char bs, char tl, char tr, char bl, char br)
{
    wborder(win, ls, rs, ts, bs, tl, tr, bl, br);
}

/* ncurses
    Adds the player char 
    at it's current location
*/
void screenRender::player() { mvwaddch(Screen::rogue_window, Screen::line, Screen::column, Screen::player); }

/* ncurses
    Adds the player char 
    at it's current location
*/
void screenRender::player(int y, int x) { mvwaddch(Screen::rogue_window, y, x, Screen::player); }  